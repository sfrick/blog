package de.awacademy.blog.kommentar;

import javax.validation.constraints.NotEmpty;

public class KommentarDTO {

    @NotEmpty
    private String text;

    public KommentarDTO(@NotEmpty String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }


}
