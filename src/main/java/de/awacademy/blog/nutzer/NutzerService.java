package de.awacademy.blog.nutzer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class NutzerService {
    private NutzerRepository nutzerRepository;

    @Autowired
    public NutzerService(NutzerRepository nutzerRepository) {
        this.nutzerRepository = nutzerRepository;
    }


    public void addNutzer(Nutzer nutzer) {
        nutzerRepository.save(nutzer);
    }

    public boolean nutzerExistiert(String name) {
        return nutzerRepository.existsByName(name);
    }

    public List<Nutzer> getList() {
        return nutzerRepository.findAll();
    }

    public Optional<Nutzer> getNutzerById(long id) {
        return nutzerRepository.findNutzerById(id);
    }

}
