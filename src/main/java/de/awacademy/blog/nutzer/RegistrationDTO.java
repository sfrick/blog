package de.awacademy.blog.nutzer;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class RegistrationDTO {

    @NotEmpty
    private String name;

    @Size(min = 5)
    private String passwort1;
    private String passwort2;


    public RegistrationDTO(String name, String passwort1, String passwort2) {
        this.name = name;
        this.passwort1 = passwort1;
        this.passwort2 = passwort2;
    }

    public String getName() {
        return name;
    }

    public String getPasswort1() {
        return passwort1;
    }

    public String getPasswort2() {
        return passwort2;
    }
}
