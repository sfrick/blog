package de.awacademy.blog;

import de.awacademy.blog.beitrag.Beitrag;
import de.awacademy.blog.beitrag.BeitragDTO;
import de.awacademy.blog.beitrag.BeitragService;
import de.awacademy.blog.nutzer.Nutzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.time.Instant;


@Controller
public class HomeController {

    @Autowired
    private BeitragService beitragService;


    @GetMapping("/")
    public String index(@ModelAttribute("sessionUser") Nutzer sessionUser, Model model) {
        model.addAttribute("list", beitragService.getBeitragList());
        return "index";

    }

}
