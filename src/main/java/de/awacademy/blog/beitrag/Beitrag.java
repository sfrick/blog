package de.awacademy.blog.beitrag;

import de.awacademy.blog.nutzer.Nutzer;

import javax.persistence.*;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

@Entity
public class Beitrag {

    @Id
    @GeneratedValue
    private long id;
    @ManyToOne
    private Nutzer nutzer;
    @Lob
    private String ueberschrift;
    @Lob
    private String text;
    private Instant datum;

    public Beitrag() {
    }

    public Beitrag(Nutzer nutzer, String ueberschrift, String text, Instant datum) {
        this.nutzer = nutzer;
        this.ueberschrift = ueberschrift;
        this.text = text;
        this.datum = datum;
    }

    public long getId() {
        return id;
    }

    public Nutzer getNutzer() {
        return nutzer;
    }

    public String getUeberschrift() {
        return ueberschrift;
    }

    public String getText() {
        return text;
    }

    public Instant getDatum() {
        return datum;
    }

    public String getFormattedDatum() {
        DateTimeFormatter formatter =
                DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
                        .withLocale(Locale.GERMAN)
                        .withZone(ZoneId.systemDefault());
        return formatter.format(datum);
    }

    public void setNutzer(Nutzer nutzer) {
        this.nutzer = nutzer;
    }

    public void setUeberschrift(String ueberschrift) {
        this.ueberschrift = ueberschrift;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setDatum(Instant datum) {
        this.datum = datum;
    }
}
