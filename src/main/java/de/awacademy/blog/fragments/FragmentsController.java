package de.awacademy.blog.fragments;


import de.awacademy.blog.nutzer.Nutzer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class FragmentsController {

    @GetMapping("/header")
    public String getHome(@ModelAttribute("sessionUser") Nutzer sessionUser, Model model) {
        return "header.html";
    }
}
