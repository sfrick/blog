package de.awacademy.blog.kommentar;

import de.awacademy.blog.beitrag.Beitrag;
import de.awacademy.blog.nutzer.Nutzer;

import javax.persistence.*;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

@Entity
public class Kommentar {

    @Id
    @GeneratedValue
    private long id;
    @ManyToOne
    private Beitrag beitrag;
    @Lob
    private String text;
    private Instant datum;

    @ManyToOne
    private Nutzer nutzer;


    public Kommentar() {
    }

    public Kommentar(Nutzer nutzer, Beitrag beitrag, String text, Instant datum) {
        this.nutzer = nutzer;
        this.beitrag = beitrag;
        this.text = text;
        this.datum = datum;
    }

    public long getId() {
        return id;
    }

    public Beitrag getBeitrag() {
        return beitrag;
    }

    public String getText() {
        return text;
    }

    public Instant getDatum() {
        return datum;
    }

    public String getFormattedDatum() {
        DateTimeFormatter formatter =
                DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
                        .withLocale(Locale.GERMAN)
                        .withZone(ZoneId.systemDefault());
        return formatter.format(datum);
    }

    public Nutzer getNutzer() {
        return nutzer;
    }

    public void setBeitrag(Beitrag beitrag) {
        this.beitrag = beitrag;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setDatum(Instant datum) {
        this.datum = datum;
    }

    public void setNutzer(Nutzer nutzer) {
        this.nutzer = nutzer;
    }
}
