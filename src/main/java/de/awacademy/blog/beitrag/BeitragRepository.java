package de.awacademy.blog.beitrag;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BeitragRepository extends CrudRepository<Beitrag, Long> {
    List<Beitrag> findAll();

    List<Beitrag> findAllByOrderByDatumDesc();
}
