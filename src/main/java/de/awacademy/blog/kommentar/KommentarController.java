package de.awacademy.blog.kommentar;

import de.awacademy.blog.beitrag.BeitragService;
import de.awacademy.blog.nutzer.Nutzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.time.Instant;

@Controller
public class KommentarController {

    private BeitragService beitragService;
    private KommentarService kommentarService;

    @Autowired
    public KommentarController(BeitragService beitragService, KommentarService kommentarService) {
        this.beitragService = beitragService;
        this.kommentarService = kommentarService;
    }

    @PostMapping("/{beitragId}")
    public String addKommentar(@Valid @ModelAttribute("kommentarDTO") KommentarDTO kommentarDTO,
                               @ModelAttribute("sessionUser") Nutzer sessionUser,
                               @PathVariable Long beitragId, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "/{beitragId}";
        }
        Kommentar neuerKommentar = new Kommentar(sessionUser, beitragService.findBeitrag(beitragId), kommentarDTO.getText(), Instant.now());
        kommentarService.addKommentar(neuerKommentar);
        return "redirect:/{beitragId}";
    }

    @PostMapping("/deleteKommentar/{kommentarId}")
    public String deleteKommentar(@ModelAttribute("sessionUser") Nutzer sessionUser,
                                  @PathVariable Long kommentarId, RedirectAttributes redirectAttributes) {
        Kommentar kommentar = kommentarService.findKommentar(kommentarId);
        Long beitragId = kommentar.getBeitrag().getId();
        kommentarService.deleteKommentar(kommentar);
        redirectAttributes.addAttribute("beitragId", beitragId);
        return "redirect:/{beitragId}";
    }
}
