package de.awacademy.blog.beitrag;

import de.awacademy.blog.nutzer.Nutzer;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class BeitragDTO {

    private Long id;
    @NotEmpty
    private String ueberschrift;
    @NotEmpty
    private String text;
    private Nutzer nutzer;

    public BeitragDTO(long id, String ueberschrift, String text) {
        this.id = id;
        this.ueberschrift = ueberschrift;
        this.text = text;
    }

    public String getUeberschrift() {
        return ueberschrift;
    }

    public String getText() {
        return text;
    }

    public long getId() {
        return id;
    }

    public Nutzer getNutzer() {
        return nutzer;
    }

    public void setNutzer(Nutzer nutzer) {
        this.nutzer = nutzer;
    }
}
