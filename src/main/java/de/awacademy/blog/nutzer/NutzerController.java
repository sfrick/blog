package de.awacademy.blog.nutzer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class NutzerController {

    private NutzerService nutzerService;

    @Autowired
    public NutzerController(NutzerService nutzerService) {
        this.nutzerService = nutzerService;
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("registrierungDTO", new RegistrationDTO("", "", ""));
        return "register";
    }


    @PostMapping("/register")
    public String register(@Valid @ModelAttribute("registrierungDTO") RegistrationDTO registrierungDTO, BindingResult bindingResult) {

        if (!registrierungDTO.getPasswort1().equals(registrierungDTO.getPasswort2())) {
            bindingResult.addError(new FieldError("registrierungDTO", "passwort2", "Passwörter stimmen nicht überein"));
        }

        if (nutzerService.nutzerExistiert(registrierungDTO.getName())) {
            bindingResult.addError(new FieldError("registrierungDTO", "name", "Benutzername existiert bereits"));
        }
        if (bindingResult.hasErrors()) {
            return "register";
        }
        Nutzer nutzer = new Nutzer(registrierungDTO.getName(), registrierungDTO.getPasswort1());
        nutzerService.addNutzer(nutzer);

        return "redirect:/";
    }

}
