package de.awacademy.blog.beitrag;

import de.awacademy.blog.kommentar.Kommentar;
import de.awacademy.blog.kommentar.KommentarDTO;
import de.awacademy.blog.kommentar.KommentarService;
import de.awacademy.blog.nutzer.Nutzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.time.Instant;
import java.util.List;

@Controller
public class BeitragController {

    private BeitragService beitragService;
    private KommentarService kommentarService;


    @Autowired
    public BeitragController(BeitragService beitragService, KommentarService kommentarService) {
        this.beitragService = beitragService;
        this.kommentarService = kommentarService;
    }


    @GetMapping("/{beitragId}")
    public String beitrag(@ModelAttribute("sessionUser") Nutzer sessionUser, @PathVariable Long beitragId, Model model) {
        Beitrag beitrag = beitragService.findBeitrag(beitragId);
        model.addAttribute("beitrag", beitrag);
        model.addAttribute("kommentarDTO", new KommentarDTO(""));
        List<Kommentar> kommentarList = kommentarService.getKommentarList(beitragId);
        model.addAttribute("kommentarList", kommentarList);
        return "beitrag";
    }

    @PostMapping("/")
    public String addBeitrag(@Valid @ModelAttribute("beitragDTO") BeitragDTO beitragDTO,
                             BindingResult bindingResult,
                             @ModelAttribute("sessionUser") Nutzer sessionUser) {
        if (bindingResult.hasErrors()) {
            return "index";
        }
        Beitrag neuerBeitrag = new Beitrag(sessionUser, beitragDTO.getUeberschrift(), beitragDTO.getText(), Instant.now());
        beitragService.addBeitrag(neuerBeitrag);
        return "redirect:/";
    }

    @PostMapping("/deleteBeitrag/{beitragId}")
    public String deleteBeitrag(@ModelAttribute("sessionUser") Nutzer sessionUser,
                                @PathVariable Long beitragId) {
        Beitrag beitrag = beitragService.findBeitrag(beitragId);
        for (Kommentar kommentar : kommentarService.getKommentarList(beitragId)) {
            kommentarService.deleteKommentar(kommentar);
        }
        beitragService.deleteBeitrag(beitrag);
        return "redirect:/";
    }

    @GetMapping("/changeBeitrag/{beitragId}")
    public String changeBeitrag(@ModelAttribute("sessionUser") Nutzer sessionUser, @PathVariable Long beitragId, Model model) {

        Beitrag beitrag = beitragService.findBeitrag(beitragId);
        BeitragDTO beitragDTO = new BeitragDTO(beitrag.getId(), beitrag.getUeberschrift(), beitrag.getText());
        beitragDTO.setNutzer(sessionUser);

        model.addAttribute("beitragDTO", beitragDTO);
        List<Kommentar> kommentarList = kommentarService.getKommentarList(beitragId);
        model.addAttribute("kommentarList", kommentarList);
        return "beitragBearbeiten";
    }

    @PostMapping("/changeBeitrag/{beitragId}")
    public String changeBeitrag(@ModelAttribute("beitragDTO") BeitragDTO beitragDTO, BindingResult bindingResult,
                                @ModelAttribute("sessionUser") Nutzer sessionUser, @PathVariable Long beitragId) {
        if (bindingResult.hasErrors()) {
            return "index";
        }

        Beitrag beitrag = beitragService.findBeitrag(beitragId);
        beitrag.setUeberschrift(beitragDTO.getUeberschrift());
        beitrag.setText(beitragDTO.getText());
        beitragService.addBeitrag(beitrag);

        return "redirect:/{beitragId}";
    }

    @GetMapping("/newBeitrag")
    public String newBeitrag(@ModelAttribute("sessionUser") Nutzer sessionUser, Model model, RedirectAttributes redirectAttributes) {

        Beitrag beitrag = new Beitrag(sessionUser, "", "", Instant.now());
        beitragService.addBeitrag(beitrag);
//        beitragService.findBeitrag();
        redirectAttributes.addAttribute("beitragId", beitrag.getId());
        return "redirect:/changeBeitrag/{beitragId}";
    }

}
