package de.awacademy.blog.settings;

import de.awacademy.blog.nutzer.Nutzer;
import de.awacademy.blog.nutzer.NutzerService;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;

import javax.swing.text.StyledEditorKit;
import java.util.HashMap;
import java.util.List;

public class UserDTO {

    private long id;
    private boolean adminDTO;
    private String name;


    public UserDTO(long id, boolean adminDTO, String name) {
        this.id = id;
        this.adminDTO = adminDTO;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public boolean isAdminDTO() {
        return adminDTO;
    }

    public String getName() {
        return name;
    }

    public void setAdmin(boolean admin) {
        this.adminDTO = admin;
    }
}
