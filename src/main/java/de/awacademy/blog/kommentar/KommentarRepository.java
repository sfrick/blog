package de.awacademy.blog.kommentar;

import org.springframework.data.repository.CrudRepository;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.List;

public interface KommentarRepository extends CrudRepository<Kommentar, Long> {
    List<Kommentar> findKommentarByBeitragIdOrderByDatumAsc(long beitrag_id);
}
