package de.awacademy.blog.nutzer;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface NutzerRepository extends CrudRepository<Nutzer, Long> {
    Optional<Nutzer> findByNameAndPasswort(String username, String password);

    boolean existsByName(String name);

    List<Nutzer> findAll();

    Optional<Nutzer> findNutzerById(long id);
}
