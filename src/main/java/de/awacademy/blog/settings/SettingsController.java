package de.awacademy.blog.settings;


import de.awacademy.blog.nutzer.Nutzer;
import de.awacademy.blog.nutzer.NutzerService;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
public class SettingsController {

    NutzerService nutzerService;

    @Autowired
    public SettingsController(NutzerService nutzerService) {
        this.nutzerService = nutzerService;
    }


    @GetMapping("/settings")
    public String settings(Model model,
                           @ModelAttribute("sessionUser") Nutzer sessionUser) {

        if (sessionUser.isAdmin()) {
            model.addAttribute("list", nutzerService.getList());
            model.addAttribute("user", new Nutzer());
            return "settings";
        } else {
            return "redirect:/";
        }
    }

    @GetMapping("/settings/{userId}")
    public String userSettings(Model model,
                               @ModelAttribute("sessionUser") Nutzer sessionUser,
                               @PathVariable("userId") Long userId) {
        if (sessionUser.isAdmin()) {
            Optional<Nutzer> nutzerOpt = nutzerService.getNutzerById(userId);
            if (nutzerOpt.isPresent()) {
                UserDTO userDTO = new UserDTO(nutzerOpt.get().getId(), nutzerOpt.get().isAdmin(), nutzerOpt.get().getName());

                model.addAttribute("userDTO", userDTO);
                return "user";
            }
        } else {
            return "redirect:/login";
        }
        return "redirect:/";
    }

    @PostMapping("/settings/update/{userId}")
    public String updateSettings(Model model,
                                 @ModelAttribute("userDTO") UserDTO userDTO,
                                 @ModelAttribute("sessionUser") Nutzer sessionUser,
                                 @PathVariable("userId") Long id) {

        if (sessionUser.isAdmin()) {
            Optional<Nutzer> updateNutzerOpt = nutzerService.getNutzerById(id);
            Nutzer updateNutzer;
            if (updateNutzerOpt.isPresent()) {

                updateNutzer = updateNutzerOpt.get();
                updateNutzer.setAdmin(userDTO.isAdminDTO());
                nutzerService.addNutzer(updateNutzer);
            }

            return "redirect:/settings";
        } else {
            return "redirect:/";
        }

    }
}
