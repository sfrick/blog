package de.awacademy.blog.session;

import de.awacademy.blog.nutzer.Nutzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.time.Instant;
import java.util.Optional;

@ControllerAdvice
public class SessionControllerAdvice {

    private SessionRepository sessionRepository;

    @Autowired
    public SessionControllerAdvice(SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    @ModelAttribute("sessionUser")
    public Nutzer sessionUser(@CookieValue(value = "sessionId", defaultValue = "") String sessionId, Model model) {
        if (!sessionId.isEmpty()) {
            Optional<Session> optionalSession = sessionRepository.findByIdAndExpiresAtAfter(sessionId, Instant.now());

            if (optionalSession.isPresent()) {
                Session session = optionalSession.get();

                session.setExpiresAt(Instant.now().plusSeconds(7 * 24 * 60 * 60));
                sessionRepository.save(session);

                model.addAttribute("sessionUser", session.getNutzer());
                return session.getNutzer();
            }
        }
        return null;
    }
}
