package de.awacademy.blog.nutzer;

import de.awacademy.blog.beitrag.Beitrag;
import de.awacademy.blog.kommentar.Kommentar;
import de.awacademy.blog.session.Session;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;


@Entity
public class Nutzer {

    @Id
    @GeneratedValue
    private long id;

    private String name;
    private String passwort;

    private boolean admin;

    @OneToMany(mappedBy = "nutzer")
    private List<Beitrag> beitragListe;

    @OneToMany(mappedBy = "nutzer")
    private List<Kommentar> kommentarListe;

    @OneToMany(mappedBy = "nutzer")
    private List<Session> sessions;


    public Nutzer() {
    }

    public Nutzer(String name, String passwort) {
        this.name = name;
        this.passwort = passwort;
    }

    public String getName() {
        return name;
    }

    public boolean isAdmin() {
        return admin;
    }

    public List<Beitrag> getBeitragListe() {
        return beitragListe;
    }

    public List<Kommentar> getKommentarListe() {
        return kommentarListe;
    }

    public String getPasswort() {
        return passwort;
    }

    public long getId() {
        return id;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
}
