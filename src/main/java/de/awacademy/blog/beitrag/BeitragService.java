package de.awacademy.blog.beitrag;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BeitragService {

    private BeitragRepository beitragRepository;

    @Autowired
    public BeitragService(BeitragRepository beitragRepository) {
        this.beitragRepository = beitragRepository;
    }

    public List<Beitrag> getBeitragList() {
        return beitragRepository.findAllByOrderByDatumDesc();
    }

    public void addBeitrag(Beitrag beitrag) {
        beitragRepository.save(beitrag);
    }

    public Beitrag findBeitrag(long beitrag_id) {
        return beitragRepository.findById(beitrag_id).get();
    }

    public void deleteBeitrag(Beitrag beitrag) {
        beitragRepository.delete(beitrag);
    }

}
