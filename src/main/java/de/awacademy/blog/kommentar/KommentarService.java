package de.awacademy.blog.kommentar;


import de.awacademy.blog.beitrag.BeitragRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KommentarService {

    private KommentarRepository kommentarRepository;
    private BeitragRepository beitragRepository;

    @Autowired
    public KommentarService(KommentarRepository kommentarRepository, BeitragRepository beitragRepository) {
        this.kommentarRepository = kommentarRepository;
        this.beitragRepository = beitragRepository;
    }

    public List<Kommentar> getKommentarList(long beitrag_id) {
        return kommentarRepository.findKommentarByBeitragIdOrderByDatumAsc(beitrag_id);
    }

    public void addKommentar(Kommentar kommentar) {
        kommentarRepository.save(kommentar);
    }

    public Kommentar findKommentar(Long kommentarId) {
        return kommentarRepository.findById(kommentarId).orElseThrow();
    }

    public void deleteKommentar(Kommentar kommentar) {
        kommentarRepository.delete(kommentar);
    }
}
